#include "OrderOptimizer.hpp"

using std::placeholders::_1;
using geometry_msgs::msg::PoseStamped;
using visualization_msgs::msg::MarkerArray;
using cpp_order_optimizer_interfaces::msg::NextOrder;

////////////////////////////////////////////////////////////////////////////////
float distance_calculate(float x1, float y1, float x2, float y2)
{
	float x = x1 - x2;
	float y = y1 - y2;
	float dist;
	dist = pow(x, 2) + pow(y, 2);
    if(dist!=0) {
        dist = sqrt(dist); 
    }  
	return dist;
}

////////////////////////////////////////////////////////////////////////////////
OrderOptimizer::OrderOptimizer() : Node("OrderOptimizer"), current_location_{0.0, 0.0}
{
    this->declare_parameter("params_path");
    rclcpp::Parameter params_path = this->get_parameter("params_path");
    set_path_variables(params_path);
    
    sub_currentPosition_ = this->create_subscription<PoseStamped>("currentPosition", 1, std::bind(&OrderOptimizer::position_callback, this, _1));
    sub_nextOrder_ = this->create_subscription<NextOrder>("nextOrder", 1, std::bind(&OrderOptimizer::order_callback, this, _1));
    pub_orderPath_ = this->create_publisher<MarkerArray>("order_path", 1);

    this->parse_configuration();
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::publish_message()
{
    auto message = MarkerArray();
    RCLCPP_INFO(this->get_logger(), "Publishing: MarkerArray");
    pub_orderPath_->publish(message);
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::update_position(float x, float y) 
{
    current_location_.cx = x;
    current_location_.cy = y;
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::position_callback(const PoseStamped::SharedPtr message)
{
    auto header = message->header;
    auto pose = message->pose;

    float x = pose.position.x;
    float y = pose.position.y;
    RCLCPP_INFO(this->get_logger(), "Set AMR-Position to %.2f/%.2f", x, y);
    this->update_position(x, y);
    this->publish_markers();
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::order_callback(const NextOrder::SharedPtr message) 
{
    auto order_id = message->order_id;
    auto order_description = message->description;
    RCLCPP_INFO(this->get_logger(), "Working on order %d (%s)", order_id, order_description.c_str());
    this->parse_order_files(order_id);
    this->process_order();
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::set_path_variables(const rclcpp::Parameter params_path)
{
    params_path_ = params_path.as_string();
    configuration_path_ = params_path_ / "configuration";
    orders_path_ = params_path_ / "orders";

    if(!std::filesystem::exists(params_path_)) {
        RCLCPP_ERROR(this->get_logger(), "Parameter path does not exist.");
    }
    if(!std::filesystem::exists(configuration_path_)) {
        RCLCPP_ERROR(this->get_logger(), "Configuration path does not exist.");
    }
    if(!std::filesystem::exists(orders_path_)) {
        RCLCPP_ERROR(this->get_logger(), "Orders path does not exist.");
    }
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::parse_order_files(uint32_t order_id)
{
    auto order_paths = std::vector<std::string>(
        {(orders_path_ / "orders_20201201.yaml"),
         (orders_path_ / "orders_20201202.yaml"),
         (orders_path_ / "orders_20201203.yaml"),
         (orders_path_ / "orders_20201204.yaml"),
         (orders_path_ / "orders_20201205.yaml")});

    std::thread threads[NUM_THREADS];
    for(size_t i = 0; i < NUM_THREADS; i++) 
    {
        threads[i] = std::thread(parse_order_file, order_id, order_paths.at(i));
    }
    for (auto& th : threads)
    {
        th.join();
    }
    current_order_ = current_order;
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::parse_order_file(uint32_t order_id_to_find, std::string path)
{
    std::ifstream orderStream(path);
    if(!orderStream) //checking whether the file is open
    {
       return;
    }

    auto new_order = Order();
    std::string line;

    std::string order_tag = "- order:";
    while(std::getline(orderStream, line))
    {
        if(line.find(order_tag) != std::string::npos)
        {
            uint32_t order_id = std::stoi(line.substr(order_tag.size(), line.size()));
            if(order_id_to_find == order_id)
            {
                new_order.id = order_id;
                // Extract cx
                std::getline(orderStream, line, ':');
                orderStream >> new_order.destination_cx;
                // Extract cy
                std::getline(orderStream, line, ':');
                orderStream >> new_order.destination_cy;
                while(std::getline(orderStream, line))
                {
                    if(line.find(order_tag) != std::string::npos)
                    {
                        // Next Order detected
                        break;
                    }
                    else if(line.find("-") != std::string::npos)
                    {     
                        // Product detected 
                        unsigned tag = line.find('-') + 1;                        
                        uint32_t product_id = std::stoi(line.substr(tag, line.size()));
                        new_order.product_ids.push_back(product_id);
                    }
                }
                mtx.lock();
                current_order = new_order;
                mtx.unlock();
                break;
            }
        }
    }
    orderStream.close();
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::parse_configuration()
{
    std::ifstream configurationStream(configuration_path_ / "products.yaml");
    if(!configurationStream) //checking whether the file is open
    {
      RCLCPP_ERROR(this->get_logger(), "Configuration path does not exist.");
      return;
    }

    std::string id_tag = "- id:";
    std::string part_tag = "- part:";

    std::string line;
    bool first_product = true;

    auto new_product = Product();

    while(std::getline(configurationStream, line))
    {
        if(line.find(id_tag) != std::string::npos)
        {
            if(!first_product) {
                product_collection_.push_back(new_product);
                new_product = Product();
            }
            // Extract product id
            uint32_t product_id = std::stoi(line.substr(id_tag.size(), line.size()));
            new_product.id = product_id;
            // Extract product name
            std::getline(configurationStream, line);

            // +1 necessary to get position in string
            unsigned first = line.find('"') + 1;
            unsigned last = line.find_last_of('"');
            // std::remove_if(line.begin(), line.end(), isspace);
            new_product.name = line.substr(first ,last-first);
            first_product = false;
        }
        else if(line.find(part_tag) != std::string::npos)
        {
            Part new_part;
            // Extract part name
            // +1 necessary to get position in string
            unsigned first = line.find('"') + 1;
            unsigned last = line.find_last_of('"');
            // std::remove_if(line.begin(), line.end(), isspace);
            new_part.name = line.substr(first ,last-first);
            // Extract cx
            std::getline(configurationStream, line, ':');
            configurationStream >> new_part.location_cx;
            // Extract cy
            std::getline(configurationStream, line, ':');
            configurationStream >> new_part.location_cy;
            
            new_product.part_vector.push_back(new_part);
        }
    }
    // Push last product
    product_collection_.push_back(new_product);
    configurationStream.close();
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::get_parts_for_order()
{
    current_order_parts_ = std::map<std::string, FetchPart>();
    for(auto product_id : current_order_.product_ids)
    {
        Product product = product_collection_.at(product_id);
        for(auto part : product.part_vector)
        {
            // Part not in map present
            if (current_order_parts_.find(part.name) == current_order_parts_.end()) 
            {
                std::vector<uint32_t> used_in_product = std::vector<uint32_t>({product_id});
                FetchPart add_part = {part.location_cx, part.location_cy, used_in_product};
                current_order_parts_.insert(std::make_pair(part.name, add_part));
            }
            // Part is present
            else 
            {
                current_order_parts_.at(part.name).used_in_product.push_back(product_id);
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::find_shortest_path()
{
    float shortest_path = INT32_MAX;
    std::vector<std::string> part_names;
    
    // Exctract parts names
    for(auto pair_part_starting : current_order_parts_) {
        part_names.push_back(pair_part_starting.first);
    }

    // Permute over all possible configurations of Parts
    do {
        AmrLocation temp_location = current_location_;
        float current_path_length = 0;
        for(size_t i = 0; i < part_names.size(); i++) 
        {
            float next_cx = current_order_parts_[part_names.at(i)].part_location_cx;
            float next_cy = current_order_parts_[part_names.at(i)].part_location_cy;

            current_path_length += distance_calculate(temp_location.cx, temp_location.cy, next_cx, next_cy);
            temp_location = {next_cx, next_cy};
        }
        current_path_length += distance_calculate(temp_location.cx, temp_location.cy, current_order_.destination_cx, current_order_.destination_cy);
        if(current_path_length < shortest_path) 
        {
            shortest_path = current_path_length;
            shortest_path_permutation_ = part_names;
        }
    } while(std::next_permutation(part_names.begin(), part_names.end()));
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::print_order()
{
    int cnt = 1;
    for(auto fetch_part : shortest_path_permutation_)
    {
        float part_cx = current_order_parts_[fetch_part].part_location_cx;
        float part_cy = current_order_parts_[fetch_part].part_location_cy;

        for(auto for_product : current_order_parts_[fetch_part].used_in_product)
        {                
            RCLCPP_INFO(this->get_logger(), "%d. Fetching %s for product %d at x: %.2f, y: %.2f", 
            cnt++, fetch_part.c_str(), for_product, part_cx, part_cy);
        }
    }
    RCLCPP_INFO(this->get_logger(), "%d. Delivering to destination x: %.2f, y: %.2f", 
        cnt++, current_order_.destination_cx, current_order_.destination_cy);
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::publish_markers()
{
    size_t num_parts = current_order_parts_.size();
    size_t num_amr_positions = 1;

    size_t num_markers = num_parts + num_amr_positions;

    MarkerArray markerArray;
    for(size_t i = 0; i < num_markers; i++)
    {
        Marker m;
        // Common Marker Properties
        m.id = i;
        m.header.frame_id = "base_link";
        m.color.a = 1.0;
        m.color.r = 1.0;
        m.color.g = 0.0;
        m.color.b = 0.0;
        m.scale.x = 10;
        m.scale.y = 10;
        m.scale.z = 1;
        m.pose.position.z = 0.1;

        if(i==(num_markers-1)) {
            // amr position
            m.pose.position.x = current_location_.cx;
            m.pose.position.y = current_location_.cy;
            m.type = Marker::CUBE;
        }
        else 
        {
            // parts
            auto part_name = shortest_path_permutation_[i];
            float part_cx = current_order_parts_[part_name].part_location_cx;
            float part_cy = current_order_parts_[part_name].part_location_cy;
            m.pose.position.x = part_cx;
            m.pose.position.y = part_cy;
            m.type = Marker::CYLINDER;
        }
        markerArray.markers.push_back(m);
    }
    pub_orderPath_->publish(markerArray);
}

////////////////////////////////////////////////////////////////////////////////
void OrderOptimizer::process_order()
{
    if(current_order_.id)
    {
        this->get_parts_for_order();
        this->find_shortest_path();
        this->print_order();
        this->publish_markers();
        // Set location to order destination
        this->update_position(current_order_.destination_cx, current_order_.destination_cy);
        // Reset id to check if order is found
        current_order_.id = 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OrderOptimizer>());
  rclcpp::shutdown();
  return 0;
}
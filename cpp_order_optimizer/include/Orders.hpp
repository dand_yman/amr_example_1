#ifndef ORDERS_HPP
#define ORDERS_HPP

#include <vector>
#include <string>

const size_t NUM_THREADS = 5;

struct Part
{
    std::string name;
    float location_cx;
    float location_cy;
};

struct Product
{
    uint32_t id;
    std::string name;
    std::vector<Part> part_vector;

    public:
        Product() : id(0), name("New Product"), part_vector(std::vector<Part>({})) {};
        Product(uint32_t id, std::string name, std::vector<Part> part_vector) :
            id(id), name(name), part_vector(part_vector) {};
};

struct Order
{
    uint32_t id;
    std::string description;
    float destination_cx;
    float destination_cy;
    std::vector<uint32_t> product_ids;

    public:
        Order() : id(0), description("Empty"), destination_cx(0.0), destination_cy (0.0), product_ids(std::vector<uint32_t>({})) {};
        Order(uint32_t id, std::string description, float destination_cx, float destination_cy, std::vector<uint32_t> product_ids) :
            id(id), description(description), destination_cx(destination_cx), destination_cy (destination_cy), product_ids(product_ids) {};
};

Part p1 = {"Part A", 5, 1};
Part p2 = {"Part B", 2, 3};
Part p3 = {"Part C", 1, 4};

Product pr1 = {1, "Product 1", std::vector<Part>({p1, p2, p1})};
Product pr2 = {2, "Product 2", std::vector<Part>({p1, p2, p1, p2})};
Product pr3 = {3, "Product 3", std::vector<Part>({p1, p1, p1})};
Product pr4 = {4, "Product 4", std::vector<Part>({p2})};
Product pr5 = {5, "Product 5", std::vector<Part>({p3, p1, p1})};

Order o1 = {1000001, "Facy description 1", 5, 5, std::vector<uint32_t>({1, 2, 3, 4, 5})};
Order o2 = {1000002, "Facy description 2", 1, 10, std::vector<uint32_t>({2, 5, 1})};
Order o3 = {1000003, "Facy description 3", 7, 2, std::vector<uint32_t>({1, 3, 2, 5})};

auto orders = std::vector<Order>({o1, o2, o3});

#endif // ORDERS_HPP


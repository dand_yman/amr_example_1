#ifndef ORDER_OPTIMIZER_HPP
#define ORDER_OPTIMIZER_HPP

#include <map>
#include <chrono>
#include <memory>
#include <string>
#include <filesystem>
#include <algorithm>
#include <fstream>
#include <thread>
#include <mutex>
#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "visualization_msgs/msg/marker_array.hpp"
#include "visualization_msgs/msg/marker.hpp"
#include "std_msgs/msg/string.hpp"
#include "geometry_msgs/msg/pose.hpp"
#include "cpp_order_optimizer_interfaces/msg/next_order.hpp"
#include "Orders.hpp"

using geometry_msgs::msg::PoseStamped;
using visualization_msgs::msg::Marker;
using visualization_msgs::msg::MarkerArray;
using cpp_order_optimizer_interfaces::msg::NextOrder;

struct FetchPart
{
    float part_location_cx;
    float part_location_cy;
    std::vector<uint32_t> used_in_product;
};

struct AmrLocation
{
    float cx;
    float cy;
};

Order current_order;
std::mutex mtx;

float distance_calculate(float x1, float y1, float x2, float y2);

class OrderOptimizer : public rclcpp::Node
{
    public:
        OrderOptimizer();
        OrderOptimizer(OrderOptimizer& o) = delete;

    private:
        void publish_message();    
        void position_callback(const PoseStamped::SharedPtr message);
        void order_callback(const NextOrder::SharedPtr message);
        
        void parse_order_files(uint32_t order_id);
        static void parse_order_file(uint32_t order_id_to_find, std::string path);
        void parse_configuration();
        void set_path_variables(rclcpp::Parameter params_path);

        void find_shortest_path();
        void get_parts_for_order();
        void process_order();
        void print_order();
        void publish_markers();

        void update_position(float x, float y);

        rclcpp::Publisher<MarkerArray>::SharedPtr pub_orderPath_;
        rclcpp::Subscription<PoseStamped>::SharedPtr sub_currentPosition_;
        rclcpp::Subscription<NextOrder>::SharedPtr sub_nextOrder_;

        std::filesystem::path params_path_;
        std::filesystem::path configuration_path_;
        std::filesystem::path orders_path_;

        Order current_order_;
        AmrLocation current_location_;

        std::vector<Product> product_collection_;
        std::map<std::string, FetchPart> current_order_parts_;
        std::vector<std::string> shortest_path_permutation_;
};

#endif // ORDER_OPTIMIZER_HPP

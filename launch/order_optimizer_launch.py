from launch import LaunchDescription
from launch_ros.actions import Node
def generate_launch_description():
    return LaunchDescription([
        Node(
            package='cpp_order_optimizer',
            executable='OrderOptimizer',
            parameters=[
                {'params_path': '/home/dandyman/ros2_ws/amr_example_1/cpp_order_optimizer/params'},
            ],
            output='screen'
        )
    ])

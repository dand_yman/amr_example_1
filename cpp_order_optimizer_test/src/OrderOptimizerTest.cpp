#include <chrono>
#include <thread>

#include "rclcpp/rclcpp.hpp"
#include "geometry_msgs/msg/pose_stamped.hpp"
#include "cpp_order_optimizer_interfaces/msg/next_order.hpp"

using geometry_msgs::msg::PoseStamped;
using cpp_order_optimizer_interfaces::msg::NextOrder;

class OrderOptimizerTest : public rclcpp::Node
{
    public:
        OrderOptimizerTest();
        void send_single_order();
        void send_multiple_orders();
        void send_single_position();

    rclcpp::Publisher<NextOrder>::SharedPtr pub_nextOrder_;
    rclcpp::Publisher<PoseStamped>::SharedPtr pub_currentPosition_;
};

void OrderOptimizerTest::send_multiple_orders()
{
    uint32_t order_num = 1000001;
    while(true) 
    {
        pub_nextOrder_ = this->create_publisher<NextOrder>("nextOrder", 1);
        auto message = NextOrder();
        message.order_id = order_num++;
        message.description = "This is a very important order.";
        RCLCPP_INFO(this->get_logger(), "Sending Order: %d", message.order_id);
        pub_nextOrder_->publish(message);
        std::this_thread::sleep_for(std::chrono::milliseconds(10000));
    }
}

void OrderOptimizerTest::send_single_order()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    uint32_t order_num = 1000001;
    pub_nextOrder_ = this->create_publisher<NextOrder>("nextOrder", 1);
    auto message = NextOrder();
    message.order_id = order_num;
    message.description = "This is a very important order.";
    RCLCPP_INFO(this->get_logger(), "Sending Order: %d", message.order_id);
    pub_nextOrder_->publish(message);
}

void OrderOptimizerTest::send_single_position()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    pub_currentPosition_ = this->create_publisher<PoseStamped>("currentPosition", 1);
    auto message = PoseStamped();
    message.pose.position.x = 150.0;
    message.pose.position.y = 150.0;
    RCLCPP_INFO(this->get_logger(), "Sending Position: %.2f/%.2f", message.pose.position.x, message.pose.position.y);
    pub_currentPosition_->publish(message);
}

OrderOptimizerTest::OrderOptimizerTest() : Node("order_optimizer_test")
{
    this->send_single_order();
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    this->send_single_position();
}

////////////////////////////////////////////////////////////////////////////////
int main(int argc, char * argv[])
{
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<OrderOptimizerTest>());
  rclcpp::shutdown();
  return 0;
}
#!/bin/bash
colcon build --packages-select cpp_order_optimizer_interfaces
source install/setup.bash

colcon build --packages-select cpp_order_optimizer
colcon build --packages-select cpp_order_optimizer_test
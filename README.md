# amr_example_1

This Repository contains the training example (1) for my application at Knapp Industry Solutions.

A script is provided the  build the example and its dependencies:
```
bash build.sh
source install/setup.bash
```

To run the example, a launch file is provided. 
```
ros2 launch launch/order_optimizer_launch.py
```